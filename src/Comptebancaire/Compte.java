package Comptebancaire;

public class Compte {
	private int solde;
	
	public Compte() {
		this.solde = 1000;
	}
	
	public void setSolde(int number) {
		this.solde += number;
	}

	public void getSolde() {
		System.out.println("solde actuel est : " + this.solde );
	}
	
	public int verifSolde() {
		return this.solde;
	}
	
	public void crediter(int number) {
		this.setSolde(number);
		this.getSolde();
	}
	
	public void debiter(int number) {
		if(number > this.verifSolde()) {
			System.out.println("vous n'avez pas assez de fond sur votre compte bancaire");
		}
		else {
			this.setSolde(-number);
			this.getSolde();
		}
	}
	
	public static void virement(Compte C1, Compte C2,int number) {
		C1.debiter(number);
		C2.crediter(number);
		C1.getSolde();
		C2.getSolde();
	}
}
