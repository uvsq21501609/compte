package Test;
import static org.junit.Assert.*;
import org.junit.*;

import Comptebancaire.Compte;

public class CompteTest {
	Compte C1;
	Compte C2;
	
	@Test
	public void testCreditfalse(){
		C1 = new Compte();
		C1.crediter(100);
		assertEquals(1000,C1.verifSolde());
	}
	
	@Test
	public void testCredittrue(){
		C1 = new Compte();
		C1.crediter(100);
		assertEquals(1100,C1.verifSolde());
	}
	
	@Test
	public void testVirementfalse() {
		C1 = new Compte();
		C2 = new Compte();
		Compte.virement(C1, C2, 100);
		assertEquals(900,C1.verifSolde());
		assertEquals(900,C2.verifSolde());		
	}
	
	@Test
	public void testVirementtrue() {
		C1 = new Compte();
		C2 = new Compte();
		Compte.virement(C1, C2, 100);
		assertEquals(900,C1.verifSolde());
		assertEquals(1100,C2.verifSolde());		
	}
	
}
